//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";






//#line 5 "sintatico.y"
  import java.io.*;
//#line 19 "Parser.java"




public class Parser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//public class ParserVal is defined in ParserVal.java


String   yytext;//user variable to return contextual strings
ParserVal yyval; //used to return semantic vals from action routines
ParserVal yylval;//the 'lval' (result) I got from yylex()
ParserVal valstk[];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
void val_init()
{
  valstk=new ParserVal[YYSTACKSIZE];
  yyval=new ParserVal();
  yylval=new ParserVal();
  valptr=-1;
}
void val_push(ParserVal val)
{
  if (valptr>=YYSTACKSIZE)
    return;
  valstk[++valptr]=val;
}
ParserVal val_pop()
{
  if (valptr<0)
    return new ParserVal();
  return valstk[valptr--];
}
void val_drop(int cnt)
{
int ptr;
  ptr=valptr-cnt;
  if (ptr<0)
    return;
  valptr = ptr;
}
ParserVal val_peek(int relative)
{
int ptr;
  ptr=valptr-relative;
  if (ptr<0)
    return new ParserVal();
  return valstk[ptr];
}
final ParserVal dup_yyval(ParserVal val)
{
  ParserVal dup = new ParserVal();
  dup.ival = val.ival;
  dup.dval = val.dval;
  dup.sval = val.sval;
  dup.obj = val.obj;
  return dup;
}
//#### end semantic value section ####
public final static short CLASS=257;
public final static short PRIVATE=258;
public final static short PUBLIC=259;
public final static short INT=260;
public final static short BOOLEAN=261;
public final static short DOUBLE=262;
public final static short STRING=263;
public final static short VOID=264;
public final static short IF=265;
public final static short ELSE=266;
public final static short ENDIF=267;
public final static short FOR=268;
public final static short ENDFOR=269;
public final static short WHILE=270;
public final static short ENDWHILE=271;
public final static short BREAK=272;
public final static short NUM=273;
public final static short LEIA=274;
public final static short ESCREVA=275;
public final static short IDENT=276;
public final static short RETURN=277;
public final static short NEW=278;
public final static short TRUE=279;
public final static short FALSE=280;
public final static short LITERAL=281;
public final static short AND=282;
public final static short OR=283;
public final static short EQUALS=284;
public final static short LESSEQUAL=285;
public final static short GREATEREQUAL=286;
public final static short INCREMENT=287;
public final static short DECREMENT=288;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    4,    0,    3,    3,    6,    5,    7,   11,    8,    8,
    9,    9,   10,   10,   12,   12,    1,    1,    1,    1,
    1,   15,    2,   14,   14,   16,   13,   17,   17,   17,
   21,   19,   19,   18,   18,   22,   22,   23,   20,   20,
   24,   24,   24,   24,   24,   24,   24,   24,   24,   35,
   25,   34,   34,   34,   34,   40,   39,   36,   30,   30,
   29,   31,   31,   42,   42,   32,   41,   41,   43,   44,
   44,   45,   45,   37,   46,   46,   46,   46,   47,   47,
   47,   47,   48,   48,   48,   48,   48,   49,   49,   38,
   38,   50,   50,   26,   51,   51,   27,   52,   52,   53,
   53,   28,   33,   33,   33,
};
final static short yylen[] = {                            2,
    0,    2,    2,    1,    0,    6,    2,    0,    4,    0,
    3,    0,    1,    2,    1,    2,    1,    1,    1,    1,
    1,    0,    4,    1,    3,    1,    8,    1,    2,    2,
    0,    2,    0,    1,    0,    1,    3,    2,    2,    0,
    1,    1,    1,    1,    1,    2,    1,    1,    2,    0,
    4,    2,    2,    2,    0,    0,    5,    2,    4,    6,
    3,    4,    4,    2,    0,    3,    1,    0,    2,    2,
    0,    1,    1,    3,    1,    1,    1,    1,    1,    1,
    1,    1,    1,    1,    1,    1,    1,    1,    1,    3,
    1,    1,    3,    6,    3,    0,    5,    2,    0,    1,
    1,    8,    2,    2,    1,
};
final static short yydefred[] = {                         1,
    0,    0,    0,    0,    4,    5,    3,    0,    0,    0,
    0,    0,    8,    6,    0,    7,    0,    0,   17,   20,
   18,   19,   21,   22,    0,    9,    0,    0,    0,   11,
    0,    0,    0,   14,   30,   29,   16,    0,   26,    0,
    0,    0,    0,   34,    0,   23,    0,   38,    0,    0,
   25,    0,    0,   37,    0,   32,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,   42,   43,   44,   45,
    0,   47,   48,    0,    0,   75,    0,   72,   73,   78,
   76,    0,    0,   92,    0,    0,   50,    0,    0,    0,
   56,    0,    0,  103,  104,    0,    0,    0,   27,   39,
   46,   49,    0,    0,   85,   86,   87,   84,   83,    0,
    0,   79,   80,   81,   82,    0,   88,   89,    0,    0,
    0,   66,    0,    0,    0,    0,    0,    0,   61,    0,
    0,   67,    0,   93,    0,   74,   90,    0,  101,  100,
    0,    0,   64,   62,   63,    0,   52,    0,   54,   51,
    0,    0,   69,   59,    0,    0,    0,    0,  105,    0,
   97,   98,   58,   57,   70,    0,    0,   94,    0,   60,
   95,    0,  102,
};
final static short yydgoto[] = {                          1,
   24,   25,    4,    2,    5,    8,   11,   12,   16,   26,
   17,   30,   31,   40,   33,   41,   32,   43,   52,   64,
   53,   44,   45,   65,   66,   67,   68,   69,   70,   81,
   72,   73,   74,  127,   96,  147,   82,   83,   75,   97,
  131,  124,  132,  153,   84,   85,  116,  110,  119,   86,
  157,  141,  142,
};
final static short yysindex[] = {                         0,
    0, -220, -250, -220,    0,    0,    0,  -71, -200,   11,
  -44, -169,    0,    0,   34,    0, -196, -231,    0,    0,
    0,    0,    0,    0, -196,    0, -182,    0, -176,    0,
 -231,   63, -171,    0,    0,    0,    0, -196,    0,   48,
   73, -155,   88,    0,   82,    0, -171,    0,    0, -196,
    0,    8, -196,    0, -179,    0, -157, -139, -157, -138,
 -222, -232, -202,   14, -179,    0,    0,    0,    0,    0,
   81,    0,    0,   90,  -19,    0,    0,    0,    0,    0,
    0,  -59,   92,    0,   40, -168,    0, -157,   93,   94,
    0,  108,  108,    0,    0,   95,  114,   96,    0,    0,
    0,    0, -202, -117,    0,    0,    0,    0,    0, -202,
 -179,    0,    0,    0,    0, -202,    0,    0, -157,   99,
 -164,    0, -202,  101,  102, -237,  103, -202,    0,  119,
  123,    0,  125,    0, -100,    0,    0, -109,    0,    0,
 -103, -164,    0,    0,    0, -117,    0,  -59,    0,    0,
  128, -202,    0,    0, -202,  112,  -96, -232,    0,  115,
    0,    0,    0,    0,    0,  131, -179,    0, -164,    0,
    0,  -95,    0,
};
final static short yyrindex[] = {                         0,
    0,    0,    0,  175,    0,    0,    0,    0, -111,    0,
    0,   51,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0, -114,    0,    0,  -40,    0,    0,
   52,    0,    0,    0,    0,    0,    0,  137,    0,    0,
  120,    0,    0,    0,  139,    0,    0,    0, -119,    0,
    0,    0,    0,    0,   56,    0,    0,    0,    0,    0,
    0,  -21,    0,    0, -120,  -57,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,   85,    0,    0,    0,
    0,    0,    0,    0,    0,   75,    0,    0,    0,    0,
    0,  124,  124,    0,    0,  126,    0,    0,    0,    0,
    0,    0,  141,    0,    0,    0,    0,    0,    0,    0,
 -131,    0,    0,    0,    0,    0,    0,    0,    0,    0,
  -87,    0,    0,    0,    0,    0,    0,  141,    0,  145,
    0,    0,    0,    0,  -80,    0,    0,    0,    0,    0,
    0, -246,    0,    0,    0,    0,    0,  129,    0,    0,
    0,    0,    0,    0,  141,    0,    0,   17,    0,    0,
    0,    0,    0,    0,    0,    0,  -78,    0,  -79,    0,
    0,    0,    0,
};
final static short yygindex[] = {                         0,
   -3,    0,    0,    0,  187,    0,    0,    0,    0,   -5,
    0,  161,    0,  146,    0,    0,    0,    0,    0,  -58,
    0,  144,    0, -108,  -50,    0,    0,    0,    0,  -49,
    0,    0,   57,    0,    0,    0,  -53,  -42,  -86,    0,
 -106,  104,   44,    0,    0,    0,    0,    0,    0,    0,
    0, -118,    0,
};
final static int YYTABLESIZE=236;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         28,
  108,  105,  109,   33,   40,   71,  100,   88,   13,   98,
   13,   93,  140,   10,   29,   71,   89,  133,   56,   34,
  103,  151,   99,  162,   99,    6,  104,   29,   19,   20,
   21,   22,   27,  140,   42,   76,    3,   50,   77,   50,
  146,   78,   79,   80,   28,  120,   42,   56,  166,  130,
  172,    9,  135,   91,   94,   95,  134,   10,   92,  163,
  140,   71,  136,   19,   20,   21,   22,   41,   13,  143,
   76,   71,  148,   77,  130,   50,  137,   50,   80,   23,
   14,  114,  112,  149,  113,   57,  115,  159,   58,   15,
   59,   18,   71,   35,   60,   61,   62,   63,  130,   36,
   57,  130,   38,   58,   39,   59,   46,  139,  171,   60,
   61,   62,   63,  117,  118,   76,   47,   71,   77,   71,
   48,   78,   79,   80,   56,   50,   77,   77,   49,   77,
   55,   77,   91,   91,   40,   40,   87,   90,   99,  101,
   31,   31,   31,   31,   13,   40,   40,   10,  102,  111,
  121,  123,  122,  128,  129,  126,   31,  138,   91,  144,
  145,  150,  152,  154,  155,  156,  158,  161,  164,  167,
  168,  170,  169,  173,    2,   12,   15,   35,   24,   36,
   40,   68,   65,   99,   55,   71,   96,   53,   40,   99,
    7,   37,   51,   54,  160,  165,  125,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   41,   41,   41,
   41,   41,   41,   41,   41,    0,   41,   41,   41,   41,
    0,    0,    0,    0,  105,  106,  107,    0,    0,    0,
    0,    0,    0,    0,    0,   21,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         40,
   60,   59,   62,  123,  125,   55,   65,   58,  123,   63,
  125,   61,  121,  125,   18,   65,   59,  104,   40,   25,
   40,  128,  269,  142,  271,  276,   46,   31,  260,  261,
  262,  263,  264,  142,   38,  273,  257,   59,  276,   61,
  278,  279,  280,  281,  276,   88,   50,   53,  155,  103,
  169,  123,  111,  276,  287,  288,  110,  258,  281,  146,
  169,  111,  116,  260,  261,  262,  263,  125,   58,  123,
  273,  121,  126,  276,  128,   59,  119,   61,  281,  276,
  125,   42,   43,  126,   45,  265,   47,  138,  268,  259,
  270,   58,  142,  276,  274,  275,  276,  277,  152,  276,
  265,  155,   40,  268,  276,  270,   59,  272,  167,  274,
  275,  276,  277,  282,  283,  273,   44,  167,  276,  169,
  276,  279,  280,  281,   40,   44,   42,   43,   41,   45,
  123,   47,   58,   59,  266,  267,  276,  276,  125,   59,
  260,  261,  262,  263,  259,  266,  267,  259,   59,   58,
   58,   44,   59,   40,   59,   61,  276,   59,  276,   59,
   59,   59,   44,   41,   40,  266,  276,  271,   41,   58,
  267,   41,   58,  269,    0,  125,  125,   41,   59,   41,
  125,   41,   59,  271,   59,   41,  267,   59,  267,  269,
    4,   31,   47,   50,  138,  152,   93,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,  265,  266,  267,
  268,  269,  270,  271,  272,   -1,  274,  275,  276,  277,
   -1,   -1,   -1,   -1,  284,  285,  286,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  276,
};
}
final static short YYFINAL=1;
final static short YYMAXTOKEN=288;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,"'('","')'","'*'","'+'","','",
"'-'","'.'","'/'",null,null,null,null,null,null,null,null,null,null,"':'","';'",
"'<'","'='","'>'",null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
"'{'",null,"'}'",null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,"CLASS","PRIVATE","PUBLIC","INT","BOOLEAN",
"DOUBLE","STRING","VOID","IF","ELSE","ENDIF","FOR","ENDFOR","WHILE","ENDWHILE",
"BREAK","NUM","LEIA","ESCREVA","IDENT","RETURN","NEW","TRUE","FALSE","LITERAL",
"AND","OR","EQUALS","LESSEQUAL","GREATEREQUAL","INCREMENT","DECREMENT",
};
final static String yyrule[] = {
"$accept : init",
"$$1 :",
"init : $$1 class_list",
"class_list : class_list class_init",
"class_list : class_init",
"$$2 :",
"class_init : CLASS IDENT $$2 '{' class_declaration '}'",
"class_declaration : private_declaration public_declaration",
"$$3 :",
"private_declaration : PRIVATE ':' $$3 atributes",
"private_declaration :",
"public_declaration : PUBLIC ':' methods",
"public_declaration :",
"atributes : atribute",
"atributes : atribute atributes",
"methods : method",
"methods : method methods",
"type : INT",
"type : DOUBLE",
"type : STRING",
"type : BOOLEAN",
"type : IDENT",
"$$4 :",
"atribute : type $$4 ident_list ';'",
"ident_list : id",
"ident_list : id ',' ident_list",
"id : IDENT",
"method : method_type '(' parameters ')' local_var '{' cmd_list '}'",
"method_type : IDENT",
"method_type : type IDENT",
"method_type : VOID IDENT",
"$$5 :",
"local_var : $$5 atributes",
"local_var :",
"parameters : parameter_list",
"parameters :",
"parameter_list : parameter",
"parameter_list : parameter ',' parameter_list",
"parameter : type IDENT",
"cmd_list : cmd cmd_list",
"cmd_list :",
"cmd : atrib_cmd",
"cmd : if_cmd",
"cmd : while_cmd",
"cmd : for_cmd",
"cmd : return_cmd",
"cmd : method_call ';'",
"cmd : escreva_cmd",
"cmd : leia_cmd",
"cmd : increment ';'",
"$$6 :",
"atrib_cmd : IDENT $$6 atrib_cont ';'",
"atrib_cont : '=' constructor",
"atrib_cont : '=' exp",
"atrib_cont : '=' logic_exp",
"atrib_cont :",
"$$7 :",
"method_id : IDENT $$7 '(' values ')'",
"constructor : NEW method_id",
"method_call : method_id '(' values ')'",
"method_call : method_id '.' method_id '(' values ')'",
"return_cmd : RETURN exp ';'",
"escreva_cmd : ESCREVA LITERAL escreva_exp ';'",
"escreva_cmd : ESCREVA method_call escreva_exp ';'",
"escreva_exp : ',' exp",
"escreva_exp :",
"leia_cmd : LEIA IDENT ';'",
"values : value_list",
"values :",
"value_list : exp value_list_cont",
"value_list_cont : ',' value_list",
"value_list_cont :",
"bool_val : TRUE",
"bool_val : FALSE",
"exp : num_val oper exp",
"num_val : NUM",
"num_val : method_call",
"num_val : IDENT",
"num_val : LITERAL",
"oper : '+'",
"oper : '-'",
"oper : '*'",
"oper : '/'",
"num_oper : '>'",
"num_oper : '<'",
"num_oper : EQUALS",
"num_oper : LESSEQUAL",
"num_oper : GREATEREQUAL",
"logic_oper : AND",
"logic_oper : OR",
"logic_exp : logic_val logic_oper logic_exp",
"logic_exp : logic_val",
"logic_val : bool_val",
"logic_val : exp num_oper exp",
"if_cmd : IF logic_exp ':' cmd_list else_cmd ENDIF",
"else_cmd : ELSE ':' cmd_list",
"else_cmd :",
"while_cmd : WHILE logic_exp ':' loop_cmd_list ENDWHILE",
"loop_cmd_list : loop_cmd loop_cmd_list",
"loop_cmd_list :",
"loop_cmd : cmd",
"loop_cmd : BREAK",
"for_cmd : FOR atrib_cmd logic_exp ';' increment ':' loop_cmd_list ENDFOR",
"increment : IDENT INCREMENT",
"increment : IDENT DECREMENT",
"increment : atrib_cmd",
};

//#line 213 "sintatico.y"

  private Yylex lexer;
  private TabSimb ts;
   
  public static TS_entry Tp_VOID =  new TS_entry("void", null, ClasseID.TipoBase);
  public static TS_entry Tp_INT =  new TS_entry("int", null, ClasseID.TipoBase);
  public static TS_entry Tp_DOUBLE = new TS_entry("double", null,  ClasseID.TipoBase);
  public static TS_entry Tp_BOOL = new TS_entry("bool", null,  ClasseID.TipoBase);
  public static TS_entry Tp_STRING= new TS_entry("string", null,  ClasseID.TipoBase);
  public static TS_entry Tp_ERRO = new TS_entry("_erro_", null,  ClasseID.TipoBase);

  private TS_entry topo;
  private TS_entry currEscopo;
  private ClasseID currClass;
  private TS_entry currType;

  private int yylex () {
    int yyl_return = -1;
    try {
      yylval = new ParserVal(0);
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
  }


/* metodo de manipulacao de erros de sintaxe */  
public void yyerror (String error) {   
   System.err.println("Erro : " + error + " na linha " + lexer.getLine());   
   System.err.println("Entrada rejeitada");  
} 



  public Parser(Reader r) {
    lexer = new Yylex(r, this);

    ts = new TabSimb();

    //
    // não me parece que necessitem estar na TS
    // já que criei todas como public static...
    //
    ts.insert(Tp_VOID);
    ts.insert(Tp_ERRO);
    ts.insert(Tp_INT);
    ts.insert(Tp_DOUBLE);
    ts.insert(Tp_STRING);
    ts.insert(Tp_BOOL);
  }

  TS_entry validaTipo(int operador, TS_entry A, TS_entry B) {
    switch ( operador ) {
        case '=':
              if ( (A == Tp_INT && B == Tp_INT)                        ||
                    ((A == Tp_DOUBLE && (B == Tp_INT || B == Tp_DOUBLE))) ||
                    (A == B) )
                    return A;
                else
                    yyerror("(sem) tipos incomp. para atribuicao: "+ A.getTipoStr() + " = "+B.getTipoStr());
              break;

        case '+' :
              if ( A == Tp_INT && B == Tp_INT)
                    return Tp_INT;
              else if ( (A == Tp_DOUBLE && (B == Tp_INT || B == Tp_DOUBLE)) ||
                                      (B == Tp_DOUBLE && (A == Tp_INT || A == Tp_DOUBLE)) ) 
                    return Tp_DOUBLE;     
              else
                  yyerror("(sem) tipos incomp. para soma: "+ A.getTipoStr() + " + "+B.getTipoStr());
              break;

        case '>' :
                if ((A == Tp_INT || A == Tp_DOUBLE) && (B == Tp_INT || B == Tp_DOUBLE))
                    return Tp_BOOL;
                else
                  yyerror("(sem) tipos incomp. para op relacional: "+ A.getTipoStr() + " > "+B.getTipoStr());
                break;

        case AND:
                if (A == Tp_BOOL && B == Tp_BOOL)
                    return Tp_BOOL;
                else
                  yyerror("(sem) tipos incomp. para op lógica: "+ A.getTipoStr() + " && "+B.getTipoStr());
            break;
      }

      return Tp_ERRO;
           
     }

public void listarTS() { topo.listar();}

public static void main(String args[]) throws IOException {
    System.out.println("\naJava - exemplo de uso do byacc");

    Parser yyparser;
    if ( args.length > 0 ) {
      // parse a file
      yyparser = new Parser(new FileReader(args[0]));

      yyparser.yyparse();
 
      System.out.println("\nFeito!!!");
		System.out.println("\n\nListagem da tabela de simbolos:\n");
      yyparser.listarTS();
    }
    else {
      System.out.println("Uso:  java Parser <arquivo de teste>");
    }
}
//#line 532 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 23 "sintatico.y"
{currEscopo = new TS_entry("aJavaFile", null, ClasseID.Programa); topo = currEscopo;}
break;
case 5:
//#line 27 "sintatico.y"
{ TS_entry nodo = new TS_entry(val_peek(0).sval, null, ClasseID.ClasseNome);
									currEscopo.insert(nodo);
									currEscopo = nodo;
									ts.insert(nodo);
								 }
break;
case 8:
//#line 37 "sintatico.y"
{ currClass = ClasseID.AtribClass; }
break;
case 17:
//#line 53 "sintatico.y"
{ yyval.obj = Tp_INT; }
break;
case 18:
//#line 54 "sintatico.y"
{ yyval.obj = Tp_DOUBLE; }
break;
case 19:
//#line 55 "sintatico.y"
{ yyval.obj = Tp_STRING; }
break;
case 20:
//#line 56 "sintatico.y"
{ yyval.obj = Tp_BOOL; }
break;
case 21:
//#line 57 "sintatico.y"
{ TS_entry nodo = ts.pesquisa(val_peek(0).sval);
            if (nodo == null ) 
              yyerror("(sem) Nome de tipo <" + val_peek(0).sval + "> não declarado ");
            else 
              yyval.obj = nodo;
          }
break;
case 22:
//#line 65 "sintatico.y"
{
                  TS_entry nodo = ts.pesquisa( ((TS_entry)val_peek(0).obj).getTipoStr() );
                  if (nodo == null ) 
                    yyerror("(sem) Nome de tipo <" + val_peek(0).obj + "> não declarado ");
                  else 
                    currType = nodo;
                }
break;
case 26:
//#line 75 "sintatico.y"
{  TS_entry nodo = currEscopo.pesquisa(val_peek(0).sval);
              if (nodo != null) 
              	 yyerror("(sem) atributo >" + val_peek(0).sval + "< já declarado");
              else currEscopo.insert(new TS_entry(val_peek(0).sval, currType, currClass));
            }
break;
case 27:
//#line 81 "sintatico.y"
{ currEscopo = currEscopo.getPai(); }
break;
case 28:
//#line 83 "sintatico.y"
{ 	currType = Tp_VOID;
								TS_entry nodo = currEscopo.pesquisa(val_peek(0).sval);
						   	if (nodo != null) 
						   		yyerror("(sem) atributo >" + val_peek(0).sval + "< já declarado");
								else { nodo = new TS_entry(val_peek(0).sval, currType, ClasseID.NomeMetodo, currEscopo);
									currEscopo.insert(nodo);
									currEscopo = nodo;
								}
					   }
break;
case 29:
//#line 92 "sintatico.y"
{  currType = ((TS_entry)val_peek(1).obj);
									 TS_entry nodo = ts.pesquisa(val_peek(0).sval);
								    if (nodo != null) 
								      yyerror("(sem) atributo >" + val_peek(0).sval + "< já declarado");
									else { nodo = new TS_entry(val_peek(0).sval, currType, ClasseID.NomeMetodo, currEscopo);
											currEscopo.insert(nodo);
											currEscopo = nodo;
									}
								}
break;
case 30:
//#line 101 "sintatico.y"
{  currType = Tp_VOID;
									 TS_entry nodo = ts.pesquisa(val_peek(0).sval);
								    if (nodo != null) 
								      yyerror("(sem) atributo >" + val_peek(0).sval + "< já declarado");
									else { nodo = new TS_entry(val_peek(0).sval, currType, ClasseID.NomeMetodo, currEscopo);
											currEscopo.insert(nodo);
											currEscopo = nodo;
									}
								}
break;
case 31:
//#line 111 "sintatico.y"
{currClass = ClasseID.VarLocal; }
break;
case 38:
//#line 117 "sintatico.y"
{ 	currType = ((TS_entry)val_peek(1).obj);
									TS_entry nodo = currEscopo.pesquisa(val_peek(0).sval);
									if (nodo != null)
										yyerror("(sem) parametro >" + val_peek(0).sval + "< já declarado");
									else {
										currEscopo.insert(new TS_entry(val_peek(0).sval, currType, ClasseID.NomeParam));
									};
            				}
break;
case 50:
//#line 130 "sintatico.y"
{
							TS_entry nodo = currEscopo.pesquisaRec(val_peek(0).sval);
							if(nodo == null)
								yyerror("(sem) variavel >" + val_peek(0).sval + "< não declarada");
						}
break;
case 56:
//#line 141 "sintatico.y"
{
							TS_entry nodo = currEscopo.pesquisaRec(val_peek(0).sval);
							if(nodo == null || nodo.getClasse() != ClasseID.NomeMetodo)
								yyerror("(sem) metodo >" + val_peek(0).sval + "< não declarado");
						}
break;
//#line 811 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public Parser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
