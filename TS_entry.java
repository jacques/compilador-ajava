import java.util.ArrayList;

public class TS_entry
{
	private String id;
	private ClasseID classe;  
	private TS_entry tipo;
	private TabSimb locais;
	private TS_entry pai;

	public TS_entry(String umId, TS_entry umTipo, ClasseID umaClasse, TS_entry umPai) {
		id = umId;
		tipo = umTipo;
		classe = umaClasse;
		locais = new TabSimb();
		pai = umPai;
	}

	public TS_entry(String umId, TS_entry umTipo, ClasseID umaClasse){
		id = umId;
		tipo = umTipo;
		classe = umaClasse;
		locais = new TabSimb();
		pai = null;
	}

	public ClasseID getClasse(){
		return classe;	
	}

	public TS_entry getPai(){
		return pai;
	}
	
	public void setPai(TS_entry umPai) {
		pai = umPai;
	}

	public String getId() {
		return id; 
	}

	public TabSimb getLocais(){
		return locais;
	}

	public TS_entry getTipo() {
		return tipo; 
	}

	public void insert( TS_entry nodo ) {
		locais.insert(nodo);
	}

	public TS_entry pesquisa(String umId){
		return locais.pesquisa(umId);
	}

	public TS_entry pesquisaRec(String umId){
		TS_entry nodo = locais.pesquisa(umId);
		if(nodo == null && pai != null)
			return pai.pesquisaRec(umId);
		else
			return nodo;
	}

	public TS_entry pesquisa(String umId, TabSimb valores){
		return locais.pesquisa(umId, valores);
	}

	public TS_entry pesquisaRec(String umId, TabSimb valores){
		TS_entry nodo = locais.pesquisa(umId, valores);
		if(nodo == null && pai != null)
			return pai.pesquisaRec(umId, valores);
		else
			return nodo;
	}

	public void listar(){
		if(classe == ClasseID.ClasseNome){
			System.out.println("");
		}
		System.out.println(this);
		locais.listar();
	}

	public TabSimb getParam(){
		TabSimb param = new TabSimb();
		for(TS_entry v: locais.getLista()){
			if(v.getClasse() == ClasseID.NomeParam)
				param.insert(v);	
		}
		return param;
	}
    
	public String toString() {
		StringBuilder aux = new StringBuilder("");
        
		aux.append("Id: ");
		aux.append(String.format("%-10s", id));
		aux.append("\tClasse: ");
		aux.append(classe);
		aux.append("\tTipo: "); 
		aux.append(tipo2str(this.tipo));
		if (pai != null) {
			aux.append("\tPai: ");
			aux.append(pai.getId());
		}

		return aux.toString();
	}

	public String getTipoStr() {
		return tipo2str(this); 
	}

    public String tipo2str(TS_entry tipo) {
		if (tipo == null)  return "null"; 
		else if (tipo==Parser.Tp_INT)    return "int"; 
		else if (tipo==Parser.Tp_BOOL)   return "boolean"; 
		else if (tipo==Parser.Tp_DOUBLE)  return "double";
		else if (tipo==Parser.Tp_VOID)  return "void";
		else if (tipo==Parser.Tp_STRING)  return "string";
		else if (tipo==Parser.Tp_ERRO)  return  "_erro_";
		else if (tipo.getClasse() == ClasseID.ClasseNome) return tipo.getId().toString();
		else if (tipo.getClasse() == ClasseID.NomeMetodo) return tipo2str(tipo.tipo);
		else return "erro/tp --> " + tipo.getId();
   }

}