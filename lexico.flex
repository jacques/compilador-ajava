// Matheus Streb, 142021260, matheus.streb@acad.pucrs.br
// Vinicius Jacques, 111001681, viniciusjacques@gmail.com

%%

%{

  private Parser yyparser;

  public Yylex(java.io.Reader r, Parser yyparser) {
    this(r);
    this.yyparser = yyparser;
  }

  public int getLine() {
      return yyline;
  }

%} 

%byaccj

%integer
%line

NL  = \n | \r | \r\n
WHITE_SPACE_CHAR=[\n\r\ \t\b\012]
STRING_TEXT=(\\\"|[^\n\r\"]|\\{WHITE_SPACE_CHAR}+\\)*
%%


class { return Parser.CLASS;}
private { return Parser.PRIVATE;}
public { return Parser.PUBLIC;}
int { return Parser.INT;}
boolean { return Parser.BOOLEAN;}
double { return Parser.DOUBLE;}
string { return Parser.STRING;}
void { return Parser.VOID;}
if { return Parser.IF;}
else { return Parser.ELSE;}
endIf { return Parser.ENDIF;}
for { return Parser.FOR;} 
endFor { return Parser.ENDFOR;}
while { return Parser.WHILE;}
endWhile { return Parser.ENDWHILE;}
break { return Parser.BREAK;}

"return" { return Parser.RETURN;}
new { return Parser.NEW;}
true { return Parser.TRUE;}
false { return Parser.FALSE;}
"&&" { return Parser.AND;}
"||" { return Parser.OR;} 
"==" { return Parser.EQUALS;}
"<=" { return Parser.LESSEQUAL;}
">=" { return Parser.GREATEREQUAL;}
"++" { return Parser.INCREMENT;}
"--" { return Parser.DECREMENT;}

"extends" { return Parser.EXTENDS; }

[0-9]+.[0-9]+ { yyparser.yylval = new ParserVal(Double.parseDouble(yytext())); return Parser.NUM_D;}
[0-9]+ { yyparser.yylval = new ParserVal(Double.parseDouble(yytext())); return Parser.NUM;}
Leia { return Parser.LEIA;}
Escreva { return Parser.ESCREVA;}
[a-zA-Z][a-zA-Z0-9]* { yyparser.yylval = new ParserVal(yytext()); return Parser.IDENT;}

"<"	|
">"	|
":"	|
"-"	|
"/"	|
","	|
"{"	|
"}"	|
"."	|
"=" |
"(" |
")" |
";" |
"*" |
"+"     { return (int) yycharat(0); }

\"{STRING_TEXT}\" {
	String str =  yytext().substring(1,yytext().length()-1);
	yyparser.yylval = new ParserVal(str);
	return Parser.LITERAL;
} 

[ \t]+ { }
{NL}+  { } 
SL_COMMENT { }

.    { System.err.println("Error: unexpected character '"+yytext()+"' na linha "+yyline); return YYEOF; }
